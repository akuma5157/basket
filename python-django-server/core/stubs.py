"""
Do not modify this file. It is generated from the Swagger specification.
"""


class AbstractStubClass(object):
    """
    Implementations need to be derived from this class.
    """

    # getTime -- Synchronisation point for meld
    @staticmethod
    def getTime(request, *args, **kwargs):
        """
        :param request: An HttpRequest
        """
        raise NotImplementedError()


